###################################################
# build the docker file and push it to the registry
###################################################

if [ ! -v REGISTRY_SERVICE_IP ]; then
  echo No cluster IP has been chosen for the registry service, cannot build
  exit
fi

version=$(cat VERSION)
echo mongo-map version is $version

versionPresent=$(curl --cacert ../registry/certs/domain.crt \
  https://$REGISTRY_SERVICE_IP:5000/v2/mongo-map/tags/list |\
  grep \"$version\" -cs)

if [ -n "$versionPresent" ]; then
  echo mongo-map version $version is already registered. skipping build...
else
  echo building mongo map containers...
  podman build -t mongo-map:latest mongo
  podman build -t mapper:latest mapper
  podman tag mongo-map:latest $REGISTRY_SERVICE_IP:5000/mongo-map:latest
  podman tag mongo-map:latest $REGISTRY_SERVICE_IP:5000/mongo-map:$version
  podman tag mapper:latest $REGISTRY_SERVICE_IP:5000/mapper:latest
  podman tag mapper:latest $REGISTRY_SERVICE_IP:5000/mapper:$version

  echo pushing mongo-map container to registry...
  # podman expects the cert file name to end with cert
  [ ! -e ../registry/certs/domain.cert ] && ln -rs domain.crt ../registry/certs/domain.cert
  podman push --cert-dir ../registry/certs $REGISTRY_SERVICE_IP:5000/mongo-map:latest
  podman push --cert-dir ../registry/certs $REGISTRY_SERVICE_IP:5000/mongo-map:$version
  podman push --cert-dir ../registry/certs $REGISTRY_SERVICE_IP:5000/mapper:latest
  podman push --cert-dir ../registry/certs $REGISTRY_SERVICE_IP:5000/mapper:$version
fi