if [ ! -v REGISTRY_SERVICE_IP ]; then
  echo No cluster IP has been chosen for the registry service.
  REGISTRY_SERVICE_IP=10.110.26.207
  export REGISTRY_SERVICE_IP
  echo Defaulting to $REGISTRY_SERVICE_IP
fi

./build.sh

echo deploying mongo database...
kubectl create -f mongo-deployment.yaml

echo setup mongo service...
kubectl create -f mongo-service.yaml

echo setup restart job...
kubectl create -f restart-mongo-cron-job.yaml