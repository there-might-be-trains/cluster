# MongoDB instance for GTFS data
This module deploys a pod with a mongodb instance and an init container that provides the GTFS data.
Using `update.sh` it supports rolling updates from its Dockerfile and with a CronJob it supports weekly updates to 
the GTFS dataset.

### Build Lifecycle
Use `setup.sh` to bootstrap this module. It invokes `build.sh` to build both docker images in the subfolders `mongo` 
and `gtfs_data`.
The images are pushed to the `registry` container.

The pod is configured to use the `gtfs-data` image as an init container.
It downloads the GTFS data and stores them in an ephemeral volume from which the mongo database imports it.
Using a CronJob (and related roles) defined in `restart-mongo-cron-job.yaml`, the container is scheduled to restart 
once per week to download a new GTFS dataset.
The docker images can be updated by bumping the version tag in the `VERSION` file and then running `update.sh`.

Use `destroy.sh` to delete all kubernetes resources.

### Readiness
A startup probe waits for mongodb to import all data.
It awaits the creation of a file called `/data/.ready` which is created by the startup script after import.