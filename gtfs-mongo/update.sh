##################################################################################################################
# update the docker container from the changed dockerfile, push to registry and schedule rolling update
##################################################################################################################

if [ ! -v REGISTRY_SERVICE_IP ]; then
  echo No cluster IP has been chosen for the registry service.
  REGISTRY_SERVICE_IP=10.110.26.207
  export REGISTRY_SERVICE_IP
  echo Defaulting to $REGISTRY_SERVICE_IP
fi

version=$(cat VERSION)

./build.sh && \
  kubectl set image deployments/mongo-deployment mongo-gtfs=$REGISTRY_SERVICE_IP:5000/mongo-gtfs:$version
