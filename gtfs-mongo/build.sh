###################################################
# build the docker file and push it to the registry
###################################################

if [ ! -v REGISTRY_SERVICE_IP ]; then
  echo No cluster IP has been chosen for the registry service, cannot build
  exit
fi

version=$(cat VERSION)
echo mongo-gtfs version is $version

versionPresent=$(curl --cacert ../registry/certs/domain.crt \
  https://$REGISTRY_SERVICE_IP:5000/v2/mongo-gtfs/tags/list |\
  grep \"$version\" -cs)

if [ -n "$versionPresent" ]; then
  echo mongo-gtfs version $version is already registered. skipping build...
else
  echo building mongo containers...
  podman build -t mongo-gtfs:latest mongo
  podman build -t gtfs-data:latest gtfs_data
  podman tag mongo-gtfs:latest $REGISTRY_SERVICE_IP:5000/mongo-gtfs:latest
  podman tag mongo-gtfs:latest $REGISTRY_SERVICE_IP:5000/mongo-gtfs:$version
  podman tag gtfs-data:latest $REGISTRY_SERVICE_IP:5000/gtfs-data:latest
  podman tag gtfs-data:latest $REGISTRY_SERVICE_IP:5000/gtfs-data:$version

  echo pushing mongo container to registry...
  # podman expects the cert file name to end with cert
  [ ! -e ../registry/certs/domain.cert ] && ln -rs domain.crt ../registry/certs/domain.cert
  podman push --cert-dir ../registry/certs $REGISTRY_SERVICE_IP:5000/mongo-gtfs:latest
  podman push --cert-dir ../registry/certs $REGISTRY_SERVICE_IP:5000/mongo-gtfs:$version
  podman push --cert-dir ../registry/certs $REGISTRY_SERVICE_IP:5000/gtfs-data:latest
  podman push --cert-dir ../registry/certs $REGISTRY_SERVICE_IP:5000/gtfs-data:$version
fi