REGISTRY_SERVICE_IP=10.110.26.207
export REGISTRY_SERVICE_IP

pushd . || exit
cd registry || exit
./setup.sh
# await registry startup
while
  readyness=$(kubectl get pods | fgrep "registry" | grep "Running" -c)
  [[ readyness -ne 1 ]]
do sleep 1s; done
popd

pushd . || exit
cd gtfs-mongo || exit
./setup.sh
popd

