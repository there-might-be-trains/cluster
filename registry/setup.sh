if [ ! -v REGISTRY_SERVICE_IP ]; then
  echo No cluster IP has been chosen for the registry service.
  REGISTRY_SERVICE_IP=10.110.26.207
  echo Defaulting to $REGISTRY_SERVICE_IP
fi

# create a self-signed certificate for the docker registry. K8s will handle distribution.
if [ ! -d certs ]; then
  echo create self-signed certificate...
  mkdir certs
  openssl req \
    -newkey rsa:4096 -nodes -sha256 -keyout certs/domain.key \
    -addext "subjectAltName = IP:$REGISTRY_SERVICE_IP" \
    -config="cert_config.cnf" \
    -x509 -days 365 -out certs/domain.crt
fi

# create a secret volume that will make the certificate available to the cluster
echo create kubernetes tls secret...
kubectl create secret tls registry-cert \
  --cert=certs/domain.crt \
  --key=certs/domain.key \

# create a persistent volume claim for the registry using the provided config.
# The setup does not provide a volume, there must be either a volume or a storage class available
echo creating persisting volume claim...
kubectl create -f registry-data-pvc.yaml

# deploy registry onto the cluster
echo deploying registry...
kubectl create -f registry-deployment.yaml

# setup a service to make the registry accessible in the network
echo setup registry service...
kubectl create -f registry-service.yaml