# Local Docker Registry
This module sets up a private docker registry within a pod in the cluster, and adds a service to access it from 
controller pods.

### Volume
It creates a persistent volume claim to persist all pushed images even when rescheduled.
It does not provide any persistent volume or storage class to bind to satisfy the claim.

### Certificate
The `setup.sh` script creates a self-signed X.509 certificate in the directory `certs`.
This certificate is deployed in a kubernetes secret volume called registry-cert.
All nodes must therefore trust this certificate.
The certificate uses `registry-service` as its common name,
so access to the registry works via `https://registry-service:5000`.  

### Alternative Subject Name
To enable access to the registry from the host (where the host-name `registry-service` is unknown),
the service is configured to use `10.110.26.207` as its cluster-IP.
The certificate has an `subjectAltName` for this IP.
